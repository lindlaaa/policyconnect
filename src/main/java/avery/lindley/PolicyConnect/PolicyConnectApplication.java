package avery.lindley.PolicyConnect;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PolicyConnectApplication {

	public static void main(String[] args) {
		SpringApplication.run(PolicyConnectApplication.class, args);
	}
}
