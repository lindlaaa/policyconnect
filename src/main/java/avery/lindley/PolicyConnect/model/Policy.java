package avery.lindley.PolicyConnect.model;


import org.springframework.data.annotation.Id;

public class Policy {

    @Id private String id;

    private String _policyNumber;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String get_policyNumber() {
        return _policyNumber;
    }

    public void set_policyNumber(String _policyNumber) {
        this._policyNumber = _policyNumber;
    }
}
