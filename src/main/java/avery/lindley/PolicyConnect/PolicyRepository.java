package avery.lindley.PolicyConnect;

import avery.lindley.PolicyConnect.model.Policy;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface PolicyRepository extends MongoRepository<Policy, String> {
}
